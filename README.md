# Uml_Groupe3
le compte rendu se nomme Dossier_UML.pdf.

Lien pour accéder au sujet :
https://jmbruel.github.io/teaching/MobileModeling.html#_etude_de_cas_miu_management_of_irit_ut2j


### Outils de conception maquette

Lien balsamiq :
https://iutblagnac.mybalsamiq.com/projects/miu2018-g3/grid

### Lien de téléchargement de la maquette intéractive

https://gitlab.com/uml-apsio-2018/uml_groupe3/raw/master/Maquette.pdf?inline=false

### Outils à voir
Lien pencil_evolus : 
https://pencil.evolus.vn/